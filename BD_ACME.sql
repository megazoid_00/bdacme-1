/******************************************************************************
**  Name: Script SQL Data Base "Acme"
**
**  Authors:	Marvin Dickson Mendia Calizaya
**				Miguel
**				Marcelo Vargas
**				Marcelo
**				Alfredo Colque
**				Jimena
**				Wilder
**
**  Date: 06/19/2018
*******************************************************************************
**                            Change History
*******************************************************************************
**   Date:          Author:                         Description:
** --------     -------------     ---------------------------------------------
** 06/19/2018   Marvin Mendia		Initial version
** 06/20/2018	Marcelo Vargas
*******************************************************************************/

USE Acme

GO
/******************************************************************************
 ******************************************************************************
 **							TABLES CREATIONS								 **
 ******************************************************************************
 ******************************************************************************/

/******************************************************************************
 **							Creating the Role table							 **
 ******************************************************************************/
PRINT 'Creating the Role table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Role]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Role](
								  Id INT IDENTITY(1,1) NOT NULL
								  ,[create_On] DATETIME CONSTRAINT DF_Create_OnRole DEFAULT GETDATE()
								  ,[update_On] DATETIME
								  ,[version] INT CONSTRAINT DF_VersionRole DEFAULT 0
								  ,[code] VARCHAR(10) CONSTRAINT NN_CodeRole NOT NULL
								  ,[description] VARCHAR(150) CONSTRAINT NN_DescriptionRole NOT NULL
								  ,[ModifiedBy] INT
								  ,CONSTRAINT PK_Role PRIMARY KEY(Id ASC)
								  );
		PRINT 'Table Role created!';
	END
ELSE
	BEGIN
		PRINT 'Table Role already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the Position table						 **
 ******************************************************************************/
PRINT 'Creating the Position table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Position]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Position](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnPosition DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionPosition DEFAULT 0
									  ,[name] VARCHAR(30) CONSTRAINT NN_NamePosition NOT NULL
									  ,[role_id] INT NOT NULL
									  ,[ModifiedBy] INT
									  ,CONSTRAINT PK_Position PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table Position created!';
	END
ELSE
	BEGIN
		PRINT 'Table Position already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the Area table							 **
 ******************************************************************************/
PRINT 'Creating the Area table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Area]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Area](
								  Id INT IDENTITY(1,1) NOT NULL
								  ,[create_On] DATETIME CONSTRAINT DF_Create_OnArea DEFAULT GETDATE()
								  ,[update_On] DATETIME
								  ,[version] INT CONSTRAINT DF_VersionArea DEFAULT 0
								  ,[code] VARCHAR(10) CONSTRAINT NN_CodeArea NOT NULL
								  ,[name] VARCHAR(30) CONSTRAINT NN_NameArea NOT NULL
								  ,[ModifiedBy] INT
								  ,CONSTRAINT PK_Area PRIMARY KEY(Id ASC)
								  );
		PRINT 'Table Area created!';
	END
ELSE
	BEGIN
		PRINT 'Table Area already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the Training table						 **
 ******************************************************************************/
PRINT 'Creating the Training table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Training]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Training](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnTraining DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionTraining DEFAULT 0
									  ,[code] VARCHAR(10) CONSTRAINT NN_CodeTraining NOT NULL
									  ,[name] VARCHAR(50) CONSTRAINT NN_NameTraining NOT NULL
									  ,[instructor] VARCHAR(50) CONSTRAINT NN_InstructorTraining NOT NULL
									  ,[area_id] INT NOT NULL
									  ,[ModifiedBy] INT
									  ,CONSTRAINT PK_Training PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table Training created!';
	END
ELSE
	BEGIN
		PRINT 'Table Training already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the Employee_Training table						 **
 ******************************************************************************/
PRINT 'Creating the Employee_Training table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Employee_Training]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Employee_Training](
												Id INT IDENTITY(1,1) NOT NULL
												,[create_On] DATETIME CONSTRAINT DF_Create_OnEmployeeTraining DEFAULT GETDATE()
												,[update_On] DATETIME
												,[version] INT CONSTRAINT DF_VersionEmployeeTraining DEFAULT 0
												,[employee_id] INT NOT NULL
												,[training_id] INT NOT NULL
												,[state] VARCHAR(10) CONSTRAINT DF_StateEmployeeTraining DEFAULT 'ACTIVO'
												,[ModifiedBy] INT
												,CONSTRAINT PK_EmployeeTraining PRIMARY KEY(Id ASC)
												);
		PRINT 'Table Employee_Training created!';
	END
ELSE
	BEGIN
		PRINT 'Table Employee_Training already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the AuditHistory table						 **
 ******************************************************************************/
PRINT 'Creating the AuditHistory table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[AuditHistory]')
		       AND type in (N'U'))
	BEGIN
	CREATE TABLE [dbo].[AuditHistory](
									  [AuditHistoryId] INT IDENTITY(1,1) NOT NULL CONSTRAINT [PK_AuditHistory] PRIMARY KEY
									  ,[TableName]		VARCHAR(50) NULL
									  ,[ColumnName]		VARCHAR(50) NULL
									  ,[ID]             INT NULL
									  ,[Date]           DATETIME NULL
									  ,[Oldvalue]       VARCHAR(MAX) NULL
									  ,[NewValue]       VARCHAR(MAX) NULL
									  ,[ModifiedBy]     INT
									  );
		PRINT 'Table AuditHistory created!';
	END
ELSE
	BEGIN
		PRINT 'Table AuditHistory already exists into the database';
	END
GO

--       ««««««««««««««««««« Marcelo Vargas »»»»»»»»»»»»»»»»»»»»
-- ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯

 --IF NOT EXISTS(SELECT * FROM DBO.SYSDATABASES WHERE NAME = 'acme')
 --    BEGIN
 --             CREATE DATABASE acme;
 --    END
	-- PRINT 'CREAR LA BD acme';
 --GO

PRINT 'Creando la tabla TYPE_CONTACT';

IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[type_contract]')
		       AND type in (N'U'))
 BEGIN
	CREATE TABLE type_contract (
		id 				INT NOT NULL IDENTITY(1,1),
		created_on		DATE NOT NULL,
		updated_on 		DATE NULL DEFAULT NULL,
		version 		INT NOT NULL,
		description		VARCHAR(50) NULL ,
		responsable		VARCHAR(50) NULL ,
		type_contract   VARCHAR(50) NOT NULL,
		CONSTRAINT PK_type_contract PRIMARY KEY (id)
	);

		PRINT 'Tabla TYPE_CONTRACT creada !!!!!!!!!...';
	END
 ELSE
	BEGIN
		PRINT 'La tabla TYPE_CONTRACT YA EXISTE en la base de datos .........';
	END
go

PRINT 'Creando la tabla  CONTRACT';

IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[contract]')
		       AND type in (N'U'))
 BEGIN
	CREATE TABLE contract (
			id               INT NOT NULL IDENTITY(1,1),
			created_on       DATE NOT NULL,
			updated_on       DATE NULL DEFAULT NULL,
			version          INT NOT NULL,
			contract_amount  VARCHAR(9) NOT NULL,
			contract_code    VARCHAR(10) NOT NULL,
			end_date         DATE NOT NULL,
			init_date        DATE,
			payment_type     VARCHAR(9) NOT NULL,
			employee_id 	 INT NOT NULL ,
			position_id  	 INT NOT NULL ,
			project_id       INT NOT NULL ,
			type_contract_id INT NOT NULL,
			CONSTRAINT PK_contract PRIMARY KEY (id),
	);

		PRINT 'Tabla CONTRACT creada !!!!!!!!!...';
	END
 ELSE
	BEGIN
		PRINT 'La tabla CONTRACT YA EXISTE en la base de datos .........';
	END
go

/******************************************************************************
 **							Creating the "Audit and SafetyRule"					                 **
 **						    Autor: Alfredo Colque Callata				                       **
 ******************************************************************************/


IF NOT EXISTS (SELECT *
   FROM sys.[objects]
   WHERE Type = 'U'
   AND object_id = OBJECT_ID('dbo.Audit')
)
BEGIN
	CREATE TABLE Audit(
				id INT IDENTITY(1,1) NOT NULL
				,create_On DATETIME CONSTRAINT DF_Create_OnAudit DEFAULT GETDATE()
				,update_On DATETIME
				,version INT CONSTRAINT DF_VersionAudit DEFAULT 0
				,auditName VARCHAR(50) NOT NULL
				,auditCode VARCHAR(50) NOT NULL
				,auditType VARCHAR(10) NOT NULL
				,auditScope VARCHAR(50) NOT NULL
				,auditObjective VARCHAR(50) NOT NULL
				,auditCriteria VARCHAR(50) NOT NULL
				,periodicity VARCHAR(10) NOT NULL
				,employeeId INT
				,AreaId INT NOT NULL
				,ModifiedBy INT
				CONSTRAINT PK_Audit PRIMARY KEY (id ASC)
	);

	PRINT 'La tabla Audit ya fue creada';
END
ELSE
 BEGIN
  PRINT 'La tabla Audit ya existe en la base de datos';
 END
GO


IF NOT EXISTS (SELECT *
   FROM sys.[objects]
   WHERE Type = 'U'
   AND object_id = OBJECT_ID('dbo.SafetyRule')
)
BEGIN
	CREATE TABLE SafetyRule(id INT IDENTITY(1,1) NOT NULL
						,create_On DATETIME CONSTRAINT DF_Create_OnSafetyRule DEFAULT GETDATE()
						,update_On DATETIME
						,version INT CONSTRAINT DF_VersionSafetyRule DEFAULT 0
						,accomplishment BIT NOT NULL
						,auditId INT NOT NULL
						,complianceMetric INT NOT NULL
						,complianceParameter INT NOT NULL
						,policyCode VARCHAR(100) NOT NULL
						,policyName VARCHAR(100) NOT NULL
						,ModifiedBy INT
						CONSTRAINT PK_SafetyRule PRIMARY KEY (id ASC)
	);

	PRINT 'La tabla SafetyRule ya fue creada!';
END
ELSE
 BEGIN
  PRINT 'La tabla SafetyRule ya existe en la base de datos';
 END
GO


/******************************************************************************
 **							Creating the Project table						 **
 ******************************************************************************/
 PRINT 'Creating the Project table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Project]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Project](pk_project_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	                                 name VARCHAR(250) NOT NULL,
									 [description] VARCHAR(1000) NULL, 
									 [date_start] DATE not null,
								     [date_end] DATE not null,
									 [create_On] DATETIME CONSTRAINT DF_Create_OnProject DEFAULT GETDATE(),
								     [update_On] DATETIME,
									 [ModifiedBy] INT,
								     [version] INT CONSTRAINT DF_VersionProject DEFAULT 0
		);
		PRINT 'Table Project created!';
	END
ELSE
	BEGIN
		PRINT 'Table Project already exists into the database';
	END
GO


/******************************************************************************
 **							Creating the ProjectArea table						 **
 ******************************************************************************/
 PRINT 'Creating the Project_Area table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Project_Area')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE Project_Area(pk_project_area_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
		                          fk_project_id INT NOT NULL,
								  fk_area_id INT NOT NULL,
					              estado VARCHAR(50) NOT NULL CHECK (estado IN('En curso', 'Terminado')),
							      [create_On] DATETIME CONSTRAINT DF_Create_OnProject_Area DEFAULT GETDATE(),
								  [update_On] DATETIME,
								  [ModifiedBy] INT,
								  [version] INT CONSTRAINT DF_VersionProject_Area DEFAULT 0
		);
		PRINT 'Table Project_Area created!';
	END
ELSE
	BEGIN
		PRINT 'Table Project_Area already exists into the database';
	END
GO

/******************************************************************************
 ******************************************************************************
 **							CREATING THE FORIGN KEY							 **
 ******************************************************************************
 ******************************************************************************/

/******************************************************************************
 **			Define the relationship between Position and Role.  			 **
 ******************************************************************************/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Position_Role]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Position]'))
ALTER TABLE [dbo].[Position]  WITH CHECK ADD
			CONSTRAINT FK_Position_Role FOREIGN KEY (role_id)
			REFERENCES [dbo].[Role](Id)
GO
ALTER TABLE [dbo].[Position] CHECK
			CONSTRAINT [FK_Position_Role]
GO

/******************************************************************************
 **			Define the relationship between Training and Area.  			 **
 ******************************************************************************/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Training_Area]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Training]'))
ALTER TABLE [dbo].[Training]  WITH CHECK ADD
			CONSTRAINT FK_Training_Area FOREIGN KEY (area_id)
			REFERENCES [dbo].[Area](Id)
GO
ALTER TABLE [dbo].[Training] CHECK
			CONSTRAINT [FK_Training_Area]
GO

/******************************************************************************
 **		 Define the relationship between Employee_Training and Training.	 **
 ******************************************************************************/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeTraining_Training]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_Training]'))
ALTER TABLE [dbo].[Employee_Training]  WITH CHECK ADD
			CONSTRAINT FK_EmployeeTraining_Training FOREIGN KEY (training_id)
			REFERENCES [dbo].[Training](Id)
GO
ALTER TABLE [dbo].[Employee_Training] CHECK
			CONSTRAINT [FK_EmployeeTraining_Training]
GO

/******************************************************************************
 **		 Define the relationship between Employee_Training and Employee.	 **
 ******************************************************************************/
/*IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_EmployeeTraining_Employee]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Employee_Training]'))
ALTER TABLE [dbo].[Employee_Training]  WITH CHECK ADD
			CONSTRAINT FK_EmployeeTraining_Employee FOREIGN KEY (employee_id)
			REFERENCES [dbo].[Employee](Id)
GO
ALTER TABLE [dbo].[Employee_Training] CHECK
			CONSTRAINT [FK_EmployeeTraining_Employee]
GO*/


-- Definicion de la relacion entre CONTRACT y TYPE_CONTRACT.
IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_type_contract_contract]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       CONSTRAINT [FK_type_contract_contract] FOREIGN KEY([type_contract_id])
REFERENCES [dbo].[type_contract] (id)

GO
ALTER TABLE [dbo].[contract] CHECK 
       CONSTRAINT [FK_type_contract_contract]
GO


-- -- Definicion de la relacion entre CONTRACT y EMPLOYEE.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_employee_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_employee_contract] FOREIGN KEY([employee_id])
-- REFERENCES [dbo].[employee] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_employee_contract]
-- GO


-- -- Definicion de la relacion entre CONTRACT y POSITION.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_position_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_position_contract] FOREIGN KEY([position_id])
-- REFERENCES [dbo].[position] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_position_contract]
-- GO


-- -- Definicion de la relacion entre CONTRACT y PROJECT.
-- IF NOT EXISTS (SELECT * FROM sys.foreign_keys 
       -- WHERE object_id = OBJECT_ID(N'[dbo].[FK_project_contract]')
       -- AND parent_object_id = OBJECT_ID(N'[dbo].[contract]'))
	   
-- ALTER TABLE [dbo].[contract]  WITH CHECK ADD  
       -- CONSTRAINT [FK_project_contract] FOREIGN KEY([project_id])
-- REFERENCES [dbo].[project] ([id])

-- GO
-- ALTER TABLE [dbo].[contract] CHECK 
       -- CONSTRAINT [FK_project_contract]
-- GO

-- Definicion de la relacion entre Audit y Area
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audit_Area]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Audit]'))
	ALTER TABLE [dbo].[Audit]  WITH CHECK ADD CONSTRAINT [FK_Audit_Area] FOREIGN KEY(areaId)
	REFERENCES [dbo].[Area] (id)
GO
	ALTER TABLE [dbo].[Audit] CHECK CONSTRAINT [FK_Audit_Area]
GO

-- Definicion de la relacion entre Audit y Employee.
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Audit_Employee]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Audit]'))
	ALTER TABLE [dbo].[Audit]  WITH CHECK ADD CONSTRAINT [FK_Audit_Employee] FOREIGN KEY(employeeId)
	REFERENCES [dbo].[Employee] (id)
GO
	ALTER TABLE [dbo].[Audit] CHECK CONSTRAINT [FK_Audit_Employee]
GO


-- Definicion de la relacion entre SafetyRule y Audit.
IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_SafetyRule_Audit]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[SafetyRule]'))
	ALTER TABLE [dbo].[SafetyRule]  WITH CHECK ADD CONSTRAINT [FK_SafetyRule_Audit] FOREIGN KEY(auditId)
	REFERENCES [dbo].[Audit] (id)
GO
	ALTER TABLE [dbo].[SafetyRule] CHECK CONSTRAINT [FK_SafetyRule_Audit]
GO

/******************************************************************************
 **			Define the relationship between Project and ProjectArea.  			 **
 ******************************************************************************/
 IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Project_ProjectArea]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Project_Area]'))
ALTER TABLE Project_Area WITH CHECK ADD CONSTRAINT [FK_Project_ProjectArea] FOREIGN KEY (fk_project_id) REFERENCES project([pk_project_id]);
GO
ALTER TABLE [dbo].[Project_Area] CHECK CONSTRAINT [FK_Project_ProjectArea]
GO


/******************************************************************************
 **			Define the relationship between Area and ProjectArea.  			 **
 ******************************************************************************/
 IF NOT EXISTS (SELECT * FROM sys.foreign_keys
       WHERE object_id = OBJECT_ID(N'[dbo].[FK_Area_ProjectArea]')
       AND parent_object_id = OBJECT_ID(N'[dbo].[Project_Area]'))
ALTER TABLE Project_Area WITH CHECK ADD CONSTRAINT [FK_Area_ProjectArea] FOREIGN KEY (fk_area_id) REFERENCES Area([Id]);
GO
ALTER TABLE [dbo].[Project_Area] CHECK CONSTRAINT [FK_Area_ProjectArea]
GO


/******************************************************************************
 **							Creating the Item table						 **
 ******************************************************************************/
PRINT 'Creating the Item table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Item]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Item](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnItem DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionItem DEFAULT 0
									  ,[name] VARCHAR(30) CONSTRAINT NN_NameItem NOT NULL
									  ,[description] VARCHAR(30) CONSTRAINT NN_ItemDescription NOT NULL
									  ,[ModifiedBy] INT
									  ,CONSTRAINT PK_Item PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table Item created!';
	END
ELSE
	BEGIN
		PRINT 'Table Item already exists into the database';
	END
GO
/******************************************************************************
 **							Creating the ItemType table						 **
 ******************************************************************************/
PRINT 'Creating the Item table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[ItemType]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[ItemType](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnItemType DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionItemType DEFAULT 0
									  ,[name] VARCHAR(30) CONSTRAINT NN_NameItemType NOT NULL
									  ,[description] VARCHAR(30) CONSTRAINT NN_ItemTypeDescription NOT NULL
									  ,[ModifiedBy] INT
									  ,CONSTRAINT PK_ItemType PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table ItemType created!';
	END
ELSE
	BEGIN
		PRINT 'Table ItemType already exists into the database';
	END
GO
/******************************************************************************
 **							Creating the Category table						 **
 ******************************************************************************/

PRINT 'Creating the Category table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[Category]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[Category](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnCategory DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionCategory DEFAULT 0
									  ,[name] VARCHAR(30) CONSTRAINT NN_NameCategory NOT NULL
									  ,[description] VARCHAR(30) CONSTRAINT NN_CategoryDescription NOT NULL
									  ,CONSTRAINT PK_Category PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table Category created!';
	END
ELSE
	BEGIN
		PRINT 'Table Category already exists into the database';
	END
GO

/******************************************************************************
 **							Creating the Sub-Category table						 **
 ******************************************************************************/
PRINT 'Creating the Sub-Category table....';
IF NOT EXISTS (SELECT 1 FROM sys.objects
		       WHERE object_id = OBJECT_ID(N'[dbo].[SubCategory]')
		       AND type in (N'U'))
	BEGIN
		CREATE TABLE [dbo].[SubCategory](
									  Id INT IDENTITY(1,1) NOT NULL
									  ,[create_On] DATETIME CONSTRAINT DF_Create_OnSubCategory DEFAULT GETDATE()
									  ,[update_On] DATETIME
									  ,[version] INT CONSTRAINT DF_VersionSubCategory DEFAULT 0
									  ,[name] VARCHAR(30) CONSTRAINT NN_NameSubCategory NOT NULL
									  ,[parentId] INT NULL
									  ,[description] VARCHAR(30) CONSTRAINT NN_SubCategoryDescription NOT NULL
									  ,FOREIGN KEY (parentId) REFERENCES Category(ID)
									  ,CONSTRAINT PK_SubCategory PRIMARY KEY(Id ASC)
									  );
		PRINT 'Table Sub-Category created!';
	END
ELSE
	BEGIN
		PRINT 'Table Sub-Category already exists into the database';
	END
GO


