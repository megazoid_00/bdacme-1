/******************************************************************************
**				   	        CREATION OF TRIGGERS		   					 **
*******************************************************************************/
/******************************************************************************
**  Author: Marvin Mendia													 **
**  Date: 06/19/2018														 **
*******************************************************************************
**                            Change History								 **
*******************************************************************************
**   Date:				 Author:                         Description:		 **
** --------			-------------        ----------------------------------- **
** 06/19/2018		Marvin Mendia			Initial version					 **
*******************************************************************************/
USE Acme
GO
PRINT 'Start of Script Execution....';
GO

/******************************************************************************
**				 Name: TG_Training(Audit)_InsertUpdate					 **
**				 Desc: Audit History for Training table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Training(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Training(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_Training(Audit)_InsertUpdate]
ON [dbo].[Training]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(name)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Training', 
           ColumnName   = 'name',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[name], 
           NewValue     = i.[name],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.name, '') != ISNULL(i.name, '');
  END
END
GO
PRINT 'Trigger [TG_Training(Audit)_InsertUpdate] created';
GO

/******************************************************************************
**				 Name: TG_Position(Audit)_InsertUpdate						 **
**				 Desc: Audit History for Position table						 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_Position(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_Position(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_Position(Audit)_InsertUpdate]
ON [dbo].[Position]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(name)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Position', 
           ColumnName   = 'name',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[name], 
           NewValue     = i.[name],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.name, '') != ISNULL(i.name, '');
  END
END
GO
PRINT 'Trigger [TG_Position(Audit)_InsertUpdate] created';
GO
/******************************************************************************
**				 Name: TG_EmployeeTraining(Audit)_InsertUpdate				 **
**				 Desc: Audit History for Employee_Training table			 **
*******************************************************************************/
IF EXISTS (SELECT * FROM sys.objects 
		WHERE object_id = OBJECT_ID(N'[dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]') 
		AND type in (N'TR'))
BEGIN
	DROP TRIGGER [dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]
END
GO
CREATE TRIGGER [dbo].[TG_EmployeeTraining(Audit)_InsertUpdate]
ON [dbo].[Employee_Training]
FOR INSERT, UPDATE
AS
BEGIN
  IF TRIGGER_NESTLEVEL(@@ProcID) > 1 
    RETURN
 
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
 
  DECLARE @CurrDate DATETIME = GETUTCDATE();
 
  IF UPDATE(employee_id) OR UPDATE(training_id)
  BEGIN
    INSERT INTO dbo.AuditHistory(TableName, 
                                 ColumnName, 
                                 ID, 
                                 Date, 
                                 OldValue, 
                                 NewValue,
								 ModifiedBy) 
    SELECT TableName    = 'Employee_Training', 
           ColumnName   = 'employee_id',
           ID1          = i.id, 
           Date         = @CurrDate, 
           OldValue     = d.[employee_id], 
           NewValue     = i.[employee_id],
           ModifiedBy   = i.ModifiedBy          
    FROM deleted d 
    FULL OUTER JOIN inserted i ON (d.id = i.id)
    WHERE ISNULL(d.employee_id, '') != ISNULL(i.employee_id, '');
  END
END
GO
PRINT 'Trigger [TG_EmployeeTraining(Audit)_InsertUpdate] created';
GO
PRINT 'End of Script Execution....';
GO